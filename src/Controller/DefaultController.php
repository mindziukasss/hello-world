<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * TODO: Add the double asterisk in the line above for this route to work
     * @Route("/", name="home")
     */
    public function index(Request $request)
    {
        $time_start = microtime(true);
        for ($i = 0; $i < 1000; $i++) {
            fopen(__DIR__, "r");
        }
        $timeIO = round(microtime(true) - $time_start, 4);

        $time_start = microtime(true);
        for ($i = 0; $i < 3; $i++) {
            file_get_contents("http://www.google.com");
        }
        $timeNetwork = round(microtime(true) - $time_start, 4);

        $time_start = microtime(true);
        for ($i = 0; $i < 10; $i++) {
            $a = array_fill(0, 1000, mt_rand(0, 1000));
            $b = array_fill(0, 1000, mt_rand(0, 1000));
            $c = [];
            foreach ($a as $h) {
                foreach ($b as $j) {
                    $c[] = $h * $j;
                }
            }
        }
        $timePHP = round(microtime(true) - $time_start, 4);

        $writeResults = fopen('results.txt', 'w');
        fwrite($writeResults, "$timeIO\"  \"$timeNetwork\"  \"$timePHP");

        $testResults = [];
        array_push($testResults, ['timeIO' => $timeIO,
            'timeNetwork' => $timeNetwork,
            'timePHP' => $timePHP]);

        return $this->render('default/index.html.twig', [
            'testResults' => $testResults]);
    }

}
